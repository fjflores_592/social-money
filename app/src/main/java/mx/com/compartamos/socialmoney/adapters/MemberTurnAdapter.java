package mx.com.compartamos.socialmoney.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import mx.com.compartamos.socialmoney.R;
import mx.com.compartamos.socialmoney.objects.Member;

public class MemberTurnAdapter extends RecyclerView.Adapter<MemberTurnAdapter.ViewHolder> {
    private List<Member> memberList;

    public MemberTurnAdapter(List<Member> memberList) {
        this.memberList = memberList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.member_turn, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Member member = memberList.get(position);
        holder.nameView.setText(member.getName().toString());
        holder.dateView.setText(member.getDate().toString());
    }

    @Override
    public int getItemCount() {
        return memberList == null ? 0 : memberList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private View view;
        private TextView nameView, dateView;

        private ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            nameView = (TextView) itemView.findViewById(R.id.name);
            dateView = (TextView) itemView.findViewById(R.id.date);
        }
    }
}
