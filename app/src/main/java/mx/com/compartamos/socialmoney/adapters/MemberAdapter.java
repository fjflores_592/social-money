package mx.com.compartamos.socialmoney.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import mx.com.compartamos.socialmoney.R;
import mx.com.compartamos.socialmoney.objects.Member;

public class MemberAdapter extends RecyclerView.Adapter<MemberAdapter.ViewHolder> {
    private List<Member> mmemberList;

    public MemberAdapter(List<Member> memberList) {
        mmemberList = memberList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.member, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Member member = mmemberList.get(position);
        holder.nameView.setText(member.getName().toString());
        holder.checkImage.setVisibility(member.isSelected() ? View.VISIBLE : View.GONE);
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                member.setSelected(!member.isSelected());
                holder.checkImage.setVisibility(member.isSelected() ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mmemberList == null ? 0 : mmemberList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private View view;
        private TextView nameView;
        private ImageView checkImage;

        private ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            nameView = (TextView) itemView.findViewById(R.id.name);
            checkImage = (ImageView) itemView.findViewById(R.id.check);
        }
    }
}
