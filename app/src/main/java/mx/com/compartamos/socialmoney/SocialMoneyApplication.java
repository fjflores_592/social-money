package mx.com.compartamos.socialmoney;

import android.app.Application;

import java.util.ArrayList;

import mx.com.compartamos.socialmoney.objects.Member;

public class SocialMoneyApplication extends Application {
    private Boolean groupExist = false;
    private ArrayList<Member> members = new ArrayList<>();

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public Boolean getGroupExist() {
        return groupExist;
    }

    public void setGroupExist(Boolean groupExist) {
        this.groupExist = groupExist;
    }

    public ArrayList<Member> getMembers() {
        return members;
    }

    public void setMembers(ArrayList<Member> members) {
        this.members = members;
    }
}
