package mx.com.compartamos.socialmoney.objects;

public class Member {
    String name, date;
    boolean selected;

    public Member(String name) {
        this.name = name;
    }

    public Member(String name, String date) {
        this.name = name;
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public String getDate() {
        return date;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
