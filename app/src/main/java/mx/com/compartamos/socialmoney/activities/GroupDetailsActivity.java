package mx.com.compartamos.socialmoney.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import mx.com.compartamos.socialmoney.R;
import mx.com.compartamos.socialmoney.SocialMoneyApplication;
import mx.com.compartamos.socialmoney.adapters.MemberTurnAdapter;
import mx.com.compartamos.socialmoney.objects.Member;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class GroupDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final SocialMoneyApplication application = (SocialMoneyApplication) getApplication();

        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.member_list);
        RecyclerView.Adapter mAdapter = new MemberTurnAdapter(getListData());
        LinearLayoutManager manager = new LinearLayoutManager(GroupDetailsActivity.this);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setAdapter(mAdapter);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                application.setGroupExist(true);
                Intent intent = new Intent(GroupDetailsActivity.this, MainActivity.class);
                intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                GroupDetailsActivity.this.startActivity(intent);
            }
        });
    }

    private List<Member> getListData() {
        ArrayList<Member> memberList = new ArrayList<>();
        memberList.add(new Member("Elena Gonzalez", "October 21th, 2018"));
        memberList.add(new Member("Margarita Perez", "October 28th, 2018"));
        memberList.add(new Member("Maria Martinez", "November 4th, 2018"));
        memberList.add(new Member("Edith Sanchez", "November 11th, 2018"));
        return memberList;
    }
}
