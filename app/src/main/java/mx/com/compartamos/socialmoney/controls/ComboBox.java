package mx.com.compartamos.socialmoney.controls;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.List;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatEditText;
import mx.com.compartamos.socialmoney.R;
import mx.com.compartamos.socialmoney.utils.StringUtils;

@SuppressWarnings("unchecked")
public class ComboBox<T> extends AppCompatEditText {

    private List<T> mItems;
    ListAdapter mSpinnerAdapter;
    private T mSelectedObject = null;

    public ComboBox(Context context) {
        super(context);
    }

    public ComboBox(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ComboBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setItems(List<T> items) {
        mItems = items;
        setAdapter(new ArrayAdapter<>(getContext(), R.layout.combo_box_item_list_content, mItems));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        setFocusable(false);
        setClickable(true);
    }

    public void setAdapter(ListAdapter adapter) {
        mSpinnerAdapter = adapter;
        configureOnClickListener();
    }

    private void configureOnClickListener() {
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());

                @SuppressLint("InflateParams")
                View dialogView = LayoutInflater.from(getContext()).inflate(R.layout.combo_box_item_list, null);
                final ListView listView = dialogView.findViewById(R.id.list);

                listView.setAdapter(mSpinnerAdapter);
                builder.setView(dialogView);
                builder.setPositiveButton(R.string.dialog_cancel, null);

                final AlertDialog dialog = builder.create();

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        T object = (T) listView.getAdapter().getItem(position);
                        mSelectedObject = object;
                        setText(object.toString());
                        setSelectedObject(object);
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });
    }

    public List<T> getItems() {
        return mItems;
    }

    public ListAdapter getAdapter() {
        return mSpinnerAdapter;
    }

    public void setSelectedObject(T object) {
        if (object != null) {
            mSelectedObject = object;
            setText(StringUtils.stringNotEmpty(object.toString()));
        }
    }

    public T getSelectedObject() {
        return mSelectedObject;
    }
}