package mx.com.compartamos.socialmoney.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import movile.com.creditcardguide.model.IssuerCode;
import movile.com.creditcardguide.view.CreditCardView;
import mx.com.compartamos.socialmoney.R;
import mx.com.compartamos.socialmoney.SocialMoneyApplication;
import mx.com.compartamos.socialmoney.adapters.MemberTurnAdapter;
import mx.com.compartamos.socialmoney.objects.Member;

import com.android.volley.toolbox.Volley;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SocialMoneyApplication application = (SocialMoneyApplication) getApplication();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, CreateGroupActivity.class);
                MainActivity.this.startActivity(intent);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        final CreditCardView creditCardView = (CreditCardView) findViewById(R.id.creditCardView);

        creditCardView.chooseFlag(IssuerCode.VISAELECTRON);
        creditCardView.setTextExpDate("12/19");
        creditCardView.setTextNumber("5555 4444 3333 1111");
        creditCardView.setTextOwner("Maria Martinez");
        creditCardView.setTextCVV("432");

        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.member_list);
        RecyclerView.Adapter mAdapter = new MemberTurnAdapter(getListData());
        LinearLayoutManager manager = new LinearLayoutManager(MainActivity.this);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setAdapter(mAdapter);

        if (application.getGroupExist()){
            TextView message = (TextView) findViewById(R.id.no_group_message);
            message.setVisibility(View.GONE);
            View planInfo = (View) findViewById(R.id.plan_information);
            planInfo.setVisibility(View.VISIBLE);
            fab.hide();
        } else {
            TextView message = (TextView) findViewById(R.id.no_group_message);
            message.setVisibility(View.VISIBLE);
            View planInfo = (View) findViewById(R.id.plan_information);
            planInfo.setVisibility(View.GONE);
            fab.show();
        }

        MaterialButton pay_button = (MaterialButton) findViewById(R.id.pay_button);
        pay_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("Test", " Button click");
                RequestQueue ExampleRequestQueue = Volley.newRequestQueue(MainActivity.this);
                String url = "http://172.20.10.9:3000/?accounNumber=4957030005123354";
                StringRequest ExampleStringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //This code is executed if the server responds, whether or not the response contains data.
                        //The String 'response' contains the server's response.
                        //You can test it by printing response.substring(0,500) to the screen.
                        Log.i("Test", " Button soidjflksdjfk");
                    }
                }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //This code is executed if there is an error.
                        Log.i("Test", error.toString());
                    }
                });

                ExampleRequestQueue.add(ExampleStringRequest);
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            finish();
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private List<Member> getListData() {
        ArrayList<Member> memberList = new ArrayList<>();
        memberList.add(new Member("Elena Gonzalez", "October 21th, 2018"));
        memberList.add(new Member("Margarita Perez", "October 28th, 2018"));
        memberList.add(new Member("Maria Martinez", "November 4th, 2018"));
        memberList.add(new Member("Edith Sanchez", "November 11th, 2018"));
        return memberList;
    }
}
