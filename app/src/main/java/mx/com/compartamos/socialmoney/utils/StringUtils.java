package mx.com.compartamos.socialmoney.utils;

public class StringUtils {

    public static String stringNotEmpty(String string) {
        return string != null && !string.trim().equals("") && !string.equals("null") ? string.trim() : "";
    }
}
