package mx.com.compartamos.socialmoney.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.Adapter;
import mx.com.compartamos.socialmoney.R;
import mx.com.compartamos.socialmoney.adapters.MemberAdapter;
import mx.com.compartamos.socialmoney.objects.Member;

public class AddMemberActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_member);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        //handleIntent(getIntent());

        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.member_list);
        Adapter mAdapter = new MemberAdapter(getListData());
        LinearLayoutManager manager = new LinearLayoutManager(AddMemberActivity.this);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setAdapter(mAdapter);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            Intent intent = new Intent(AddMemberActivity.this, GroupDetailsActivity.class);
            AddMemberActivity.this.startActivity(intent);
            }
        });

    }

    private List<Member> getListData() {
        ArrayList<Member> memberList = new ArrayList<>();
        memberList.add(new Member("Elena Gonzalez"));
        memberList.add(new Member("Margarita Perez"));
        memberList.add(new Member("Yolanda Maldonado"));
        memberList.add(new Member("Edith Sanchez"));
        memberList.add(new Member("Lourdes Garcia"));
        memberList.add(new Member("Juana Martinez"));
        return memberList;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.search, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));

        return true;
    }

}
